import {Addon, Dessert, DessertTemplateGroups} from "../app/models/dessert.model";
import {BreadcrumbItem} from "../app/vedder-ui-lib/models/breadcrumb.model";
import {OrderStage} from "../app/models/order-stage.enum";
import {Contact} from "../app/models/contact.model";
import {Delivery} from "../app/models/order.model";


export const DESSERTS: Dessert[] = [
	{
		id: "red-velvet",
		name: "Red Velvet",
		imageIds: ["1"],
		addonsIds: ["addon-1", "addon-2"],
		ingredients: ["sugar", "milk", "red dye"],
		/*sizes: [
			{
				name: "S",
				size: "small",
				number: 5,
				price: 1200
			},
			{
				name: "M",
				size: "medium",
				number: 8,
				price: 2000
			},
			{
				name: "L",
				size: "large",
				number: 10,
				price: 3000
			}
		],*/
		sizes: [
			{
				name: "S",
				size: "small",
				diameter: 18,
				mass: 1.6,
				price: 1200
			},
			{
				name: "M",
				size: "medium",
				diameter: 23,
				mass: 2,
				price: 2000
			},
			{
				name: "L",
				size: "large",
				diameter: 27,
				mass: 2.5,
				price: 3000
			}
		],
		description: "That's a pretty delicious cake which you must taste",
		makingDuration: 2,
	}
];

export const ADDONS: Addon[] = [
	{
		description: "Cheesy cream",
		id: "addon-1",
		imageIds: [],
		price: 299
	},
	{
		description: "Smooth top",
		id: "addon-2",
		imageIds: [],
		price: 159
	}
];

export const IMAGES: Map<string, string> = new Map([
	["1", "/assets/images/1.jpg"],
	["2", "/assets/images/2.jpg"]
]);

export const DESSERT_TEMPLATE_GROUPS: DessertTemplateGroups = new Map([
	["Торты", new Map([
		[{
			id: "sovet-classic",
			name: "Coветская классика",
			imageIds: ["2"]
		}, [
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			}
		]],
		[{
			id: "modern-tastes",
			name: "Современные вкусы",
			imageIds: ["2"]
		}, [
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			}
		]],
		[{
			id: "muss-cakes",
			name: "Муссовые торты",
			imageIds: ["2"]
		}, [
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			}
		]]
	])],
	["Другие десерты", new Map([
		[{
			id: "cheesecakes",
			name: "Чизкейки",
			imageIds: ["2"]
		}, [
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			}
		]],
		[{
			id: "cupcakes",
			name: "Капкейки",
			imageIds: ["2"]
		}, [
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			}
		]],
		[{
			id: "truffles",
			name: "Трюфели",
			imageIds: ["2"]
		}, [
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			},
			{
				id: "red-velvet",
				name: "Red Velvet",
				imageIds: ["1"]
			}
		]]
	])]
]);

export const ORDER_STAGES: BreadcrumbItem[] = [
	{
		id: OrderStage.Cake,
		name: "Выбор десерта",
		active: true
	},
	{
		id: OrderStage.Size,
		name: "Выбор размера",
		active: true
	},
	{
		id: OrderStage.Addons,
		name: "Выбор дополнений",
		active: false
	},
	{
		id: OrderStage.Date,
		name: "Выбор даты",
		active: false
	},
	{
		id: OrderStage.Delivery,
		name: "Выбор доставки",
		active: false
	},
	{
		id: OrderStage.Contacts,
		name: "Контакты",
		active: false
	}
];

export const CONTACTS: Contact[] = [
	{
		service: "Instagram",
		value: "@ms.ana_cakes",
		link: "https://instagram.com/ms.ana_cakes"
	},
	{
		service: "Телефон",
		value: "+79656003830",
	},
	{
		service: "WhatsApp",
		value: "+79656003830",
		link: "https://api.whatsapp.com/send?phone=79656003830"
	},
	{
		service: "Telegram",
		value: "@Ana_Y_Karma",
	},
	{
		service: "Email",
		value: "karmanjula@mail.ru",
	}
];

export const PHONE_CODE: string = "+7";

export const DELIVERY_WAYS: Delivery[] = [
	{
		id: "pickup",
		text: "Самовывоз (г. Казань, ул. Алданская 31)",
		price: 0
	}, {
		id: "taxi",
		text: "Доставка на дом (такси)",
		price: 300
	}
];
