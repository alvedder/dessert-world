export enum OrderStage {
	Cake = 1,
	Size,
	Addons,
	Date,
	Delivery,
	Contacts
}
