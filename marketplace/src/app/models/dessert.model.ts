export type DessertTemplateGroups = Map<string, Map<DessertGroupTemplate, DessertTemplate[]>>;

export interface DessertTemplate {
	id: string;
	name: string;
	imageIds: string[];
}

export interface DessertGroupTemplate extends DessertTemplate{
}

export interface Dessert extends DessertTemplate {
	sizes?: SizeInfo[];
	addonsIds?: string[];
	description: string;
	ingredients: string[];
	makingDuration?: number; // days
}

export type Size = "small" | "medium" | "large";

export interface SizeInfo {
	size: Size;
	name: string;
	diameter?: number; // sm
	mass?: number; // kg
	number?: number,
	price: number;
}

export interface Addon {
	id: string;
	description: string;
	price: number;
	imageIds?: string[];
}
