import {Addon, Dessert, SizeInfo} from "./dessert.model";

export interface Order {
	dessert: Dessert;
	size: SizeInfo;
	addons?: Addon[];
	extraAddon?: string;
	date: Date;
	delivery: Delivery;
	contacts: Contacts;
}

export interface Delivery {
	id: string;
	text: string;
	price: number;
}

export interface Contacts {
	phone: string;
	email?: string;
}
