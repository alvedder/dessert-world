import {NgModule} from "@angular/core";
import {ExtraOptions, PreloadAllModules, RouterModule, Routes} from "@angular/router";
import {MainPageContentComponent} from "./components/main-page-content/main-page-content.component";
import {AboutContentComponent} from "./components/about-content/about-content.component";
import {DessertInfoComponent} from "./components/dessert-info/dessert-info.component";


const routes: Routes = [
	{path: "", component: MainPageContentComponent},
	{path: "about", component: AboutContentComponent},
	{path: "dessert/:dessertId", component: DessertInfoComponent},
	{
		path: "dessert/:dessertId/order",
		loadChildren: () => import("./components/order/order.module").then(module => module.OrderModule)
	}
];

const routerOptions: ExtraOptions = {
	preloadingStrategy: PreloadAllModules,
	scrollPositionRestoration: "top"
};


@NgModule({
	imports: [RouterModule.forRoot(routes, routerOptions)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
