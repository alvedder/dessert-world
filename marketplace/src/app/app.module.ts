import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {HeaderComponent} from "./components/header/header.component";
import {FooterComponent} from "./components/footer/footer.component";
import {MainPageContentComponent} from "./components/main-page-content/main-page-content.component";
import {AboutContentComponent} from "./components/about-content/about-content.component";
import {DessertInfoComponent} from "./components/dessert-info/dessert-info.component";
import {CardModule} from "./vedder-ui-lib/modules/card/card.module";
import {ButtonModule} from "./vedder-ui-lib/modules/button/button.module";
import {AssortmentComponent} from "./components/assortment/assortment.component";
import {CarouselComponent} from "./components/carousel/carousel.component";
import {SlideshowModule} from "ng-simple-slideshow";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ContentService} from "./services/content.service";
import {NavigatorService} from "./services/navigator.service";
import {NotificationService} from "./services/notification.service";


@NgModule({
	declarations: [
		AppComponent,
		HeaderComponent,
		FooterComponent,
		MainPageContentComponent,
		AboutContentComponent,
		DessertInfoComponent,
		AssortmentComponent,
		CarouselComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		CardModule,
		ButtonModule,
		SlideshowModule,
	],
	providers: [
		ContentService,
		NavigatorService,
		NotificationService
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
