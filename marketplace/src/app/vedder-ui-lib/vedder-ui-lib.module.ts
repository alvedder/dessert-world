import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {BreadcrumbsModule} from "./modules/breadcrumbs/breadcrumbs.module";
import {CalendarModule} from "./modules/calendar/calendar.module";
import {CardModule} from "./modules/card/card.module";
import {CheckboxGroupFieldModule} from "./modules/checkbox-group-field/checkbox-group-field.module";
import {DropdownFieldModule} from "./modules/dropdown-field/dropdown-field.module";
import {EmailFieldModule} from "./modules/email-field/email-field.module";
import {PhoneFieldModule} from "./modules/phone-field/phone-field.module";
import {RadioGroupFieldModule} from "./modules/radio-group-field/radio-group-field.module";
import {ButtonModule} from "./modules/button/button.module";
import {LinkedBlockGroupModule} from "./modules/linked-block-group/linked-block-group.module";
import {TextAreaFieldModule} from "./modules/text-area-field/text-area-field.module";

/**
 * This module is needed just for an opportunity to export all the vedder-ui-lib modules at once
 * If you don't use the most part of the library USE SEPARATE MODULES for more performance
 */
@NgModule({
	exports: [
		CommonModule,

		BreadcrumbsModule,
		ButtonModule,
		CalendarModule,
		CardModule,
		CheckboxGroupFieldModule,
		DropdownFieldModule,
		EmailFieldModule,
		LinkedBlockGroupModule,
		PhoneFieldModule,
		RadioGroupFieldModule,
		TextAreaFieldModule
	]
})
export class VedderUiLibModule {
}
