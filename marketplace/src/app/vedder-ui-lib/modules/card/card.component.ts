import {AfterViewInit, Component, ElementRef, HostListener, Input, OnInit, Renderer2, ViewChild} from "@angular/core";

@Component({
	selector: "vui-card",
	templateUrl: "card.component.html",
	// styleUrls: ["card.component.less"]
})
export class CardComponent implements OnInit, AfterViewInit {

	@Input()
	public title: string;

	@Input()
	public imagePath: string;

	/**
	 * ratio = width / height
	 * by default the ratio equals to 1, so that the card is a square
	 */
	@Input()
	public ratio: number = 1;

	@Input()
	public focus: boolean = false;

	@ViewChild("image")
	public imageBlock: ElementRef;

	public imagePathUrl: string;

	constructor(private renderer: Renderer2) {
	}

	public ngOnInit(): void {
	}

	public ngAfterViewInit(): void {
		/**
		 * requestAnimationFrame is needed for avoiding an error:
		 * ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked.
		 */
		requestAnimationFrame(() => {
			this.imagePathUrl = `url(\"${this.imagePath}\")`;
		});
		this.calcCardHeight();
	}

	@HostListener("window:resize")
	private resize() {
		this.calcCardHeight();
	}

	/**
	 * By default the card's height equals to card's width, so that the card is a square.
	 * It's possible to change the ratio by providing the property "ratio"
	 */
	private calcCardHeight() {
		const newHeight = Number.parseFloat(this.imageBlock.nativeElement.offsetWidth) / this.ratio + "px";

		requestAnimationFrame(() => {
			this.renderer.setStyle(this.imageBlock.nativeElement, "height", newHeight);
		});
	}
}
