import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DropdownFieldComponent} from "./dropdown-field.component";


@NgModule({
	declarations: [DropdownFieldComponent],
	exports: [DropdownFieldComponent],
	imports: [
		CommonModule
	]
})
export class DropdownFieldModule {
}
