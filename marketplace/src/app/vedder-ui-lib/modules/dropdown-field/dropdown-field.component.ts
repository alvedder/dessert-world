import {Component, Input, OnInit} from "@angular/core";

@Component({
	selector: "vui-dropdown-field",
	templateUrl: "dropdown-field.component.html",
	/*
		styleUrls: ["dropdown-field.component.less"]
	*/
})
export class DropdownFieldComponent implements OnInit {

	@Input()
	public items: string[];

	constructor() {
	}

	ngOnInit(): void {
	}

}
