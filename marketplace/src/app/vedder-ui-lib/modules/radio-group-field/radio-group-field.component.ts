import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {BooleanFieldItem} from "../../models/boolean-field-item.model";


@Component({
	selector: "vui-radio-group-field",
	templateUrl: "radio-group-field.component.html",
	/*
		styleUrls: ["radio-group-field.component.less"]
	*/
})
export class RadioGroupFieldComponent implements OnInit {

	@Input()
	public name: string;

	@Input()
	public items: BooleanFieldItem[];

	@Input()
	public value: BooleanFieldItem;

	@Output()
	public valueChange: EventEmitter<BooleanFieldItem> = new EventEmitter<BooleanFieldItem>();

	constructor() {
	}

	ngOnInit(): void {
	}

	public change(item: BooleanFieldItem): void {
		this.value = item;
		this.valueChange.emit(this.value);
	}

	public itemChecked(item: BooleanFieldItem): boolean {
		return JSON.stringify(item) === JSON.stringify(this.value);
	}
}
