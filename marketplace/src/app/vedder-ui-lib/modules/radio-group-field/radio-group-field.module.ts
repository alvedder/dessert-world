import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RadioGroupFieldComponent} from "./radio-group-field.component";

@NgModule({
	declarations: [RadioGroupFieldComponent],
	exports: [RadioGroupFieldComponent],
	imports: [
		CommonModule
	]
})
export class RadioGroupFieldModule {
}
