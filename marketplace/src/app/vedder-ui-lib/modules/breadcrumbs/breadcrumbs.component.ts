import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {BreadcrumbItem} from "../../models/breadcrumb.model";

@Component({
	selector: "vui-breadcrumbs",
	templateUrl: "breadcrumbs.component.html",
	// styleUrls: ["breadcrumbs.component.less"]
})
export class BreadcrumbsComponent implements OnInit {

	@Input()
	public items: BreadcrumbItem[];

	@Input()
	public currentItem: BreadcrumbItem;

	@Output()
	public currentItemChange: EventEmitter<BreadcrumbItem> = new EventEmitter<BreadcrumbItem>();

	constructor() {
	}

	ngOnInit(): void {
	}

	public onCurrentItemChange(item: BreadcrumbItem) {
		if (item.active) {
			if (item.customClickAction) {
				item.customClickAction();
			} else {
				this.currentItemChange.emit(item);
			}
		}
	}
}
