import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {EmailFieldComponent} from "./email-field.component";
import {NgxMaskModule} from "ngx-mask";
import {FormsModule} from "@angular/forms";


@NgModule({
	declarations: [EmailFieldComponent],
	exports: [EmailFieldComponent],
	imports: [
		CommonModule,
		NgxMaskModule.forRoot(),
		FormsModule
	]
})
export class EmailFieldModule {
}
