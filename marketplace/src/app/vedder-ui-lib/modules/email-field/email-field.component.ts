import {Component, ElementRef, ViewChild} from "@angular/core";
import {AbstractTextFieldComponent} from "../../components/abstract-text-field.component";

@Component({
	selector: "vui-email-field",
	templateUrl: "email-field.component.html",
	//	styleUrls: ["email-field.component.less"]
})
export class EmailFieldComponent extends AbstractTextFieldComponent {

	@ViewChild("wrapper")
	protected wrapper: ElementRef;

	@ViewChild("input")
	protected input: ElementRef;

	constructor() {
		super();
	}
}
