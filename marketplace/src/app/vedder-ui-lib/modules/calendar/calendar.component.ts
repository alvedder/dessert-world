import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {
	addDays,
	addMonths,
	addWeeks,
	endOfMonth,
	format,
	isSameDay,
	isToday,
	startOfMonth,
	startOfWeek,
	subDays,
	subMonths
} from "date-fns";
import {enUS} from "date-fns/locale";

type Week = Array<Date>;

@Component({
	selector: "vui-calendar",
	templateUrl: "calendar.component.html",
	/*
		styleUrls: ["calendar.component.less"]
	*/
})
export class CalendarComponent implements OnInit {

	private readonly DAYS_IN_WEEK: number = 7;
	private readonly WEEKS_TO_VIEW: number = 6;

	/**
	 * Used locales from "date-fns/locale"
	 * By default enUS
	 */
	@Input()
	public locale: Locale = enUS;

	/**
	 * By default yesterday
	 */
	@Input()
	public minDate: Date = subDays(new Date(), 1);

	/**
	 * By default a year further
	 */
	@Input()
	public maxDate: Date = addMonths(new Date(), 12);

	@Input()
	public unavailableDates: Date[] = [];

	@Output()
	public chosenDateChange: EventEmitter<Date> = new EventEmitter<Date>();

	public viewDate: Date = new Date();

	public activeDate: Date = null;

	public viewDateWeeks: Array<Week>;

	private _chosenDate: Date;

	constructor() {
	}

	@Input()
	public set chosenDate(value: Date) {
		this._chosenDate = value;
		this.activeDate = value;
	}

	public get chosenDate(): Date {
		return this._chosenDate;
	}

	public ngOnInit(): void {
		this.updateViewDateWeeks();
	}

	public incrementMonth(): void {
		const nextMonth: Date = addMonths(this.viewDate, 1);
		if (startOfMonth(nextMonth) < this.maxDate) {
			this.viewDate = nextMonth;
			this.updateViewDateWeeks();
		}
	}

	public decrementMonth(): void {
		const prevMonth = subMonths(this.viewDate, 1);
		if (this.minDate < endOfMonth(prevMonth)) {
			this.viewDate = prevMonth;
			this.updateViewDateWeeks();
		}
	}

	/**
	 * @param day is represented by a number where 0 is Sunday
	 * @param formatValue by default 2-letters names "EEEEEE" ("Mon", "Tue", ...).
	 * To set full names the format should be "EEEE"
	 * To set 1-letter names the format should be "EEEEE"
	 * To set 3-letters names the format should be "EEE"
	 */
	public getLocalizedDayOfWeek(day: (1 | 2 | 3 | 4 | 5 | 6 | 0), formatValue: string = "EEEEEE"): string {
		return format(startOfWeek(new Date(), {weekStartsOn: day}), formatValue, {locale: this.locale});
	}

	public getLocalizedViewDate(): string {
		return format(this.viewDate, "LLLL yyyy", {locale: this.locale});
	}

	public dayClick(date: Date) {
		if (date.getMonth() === this.viewDate.getMonth() && this.dateValid(date)) {
			this.activeDate = date;
			this.chosenDateChange.emit(date);
		}
	}

	public dateValid(date: Date): boolean {
		return (this.minDate <= date) &&
			(date <= this.maxDate) &&
			(this.unavailableDates.every((unavailableDate: Date) => !isSameDay(unavailableDate, date)));
	}

	public today(date: Date): boolean {
		return isToday(date);
	}

	public returnToCurrentMonth() {
		this.viewDate = new Date();
		this.updateViewDateWeeks();
	}

	private updateViewDateWeeks(): void {
		const firstDay: Date = startOfMonth(this.viewDate);
		this.viewDateWeeks = new Array<Week>(this.WEEKS_TO_VIEW)
			.fill(new Array<Date>())
			.map((week: Week, weekIndex: number) => {
				let mondayOfCurrentWeek: Date = startOfWeek(addWeeks(firstDay, weekIndex), {weekStartsOn: 1});
				return new Array<Date>(this.DAYS_IN_WEEK)
					.fill(mondayOfCurrentWeek)
					.map((date: Date, dayIndex: number) => addDays(date, dayIndex));
			});
	}
}
