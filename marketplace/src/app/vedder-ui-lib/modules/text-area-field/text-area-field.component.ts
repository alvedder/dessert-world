import {Component, ElementRef, Input, ViewChild} from "@angular/core";
import {AbstractTextFieldComponent} from "../../components/abstract-text-field.component";

@Component({
	selector: "vui-text-area-field",
	templateUrl: "text-area-field.component.html",
	// styleUrls: ["text-area-field.component.less"]
})
export class TextAreaFieldComponent extends AbstractTextFieldComponent {

	@Input()
	public placeholder: string;

	@ViewChild("wrapper")
	protected wrapper: ElementRef;

	@ViewChild("input")
	protected input: ElementRef;

	constructor() {
		super();
	}
}
