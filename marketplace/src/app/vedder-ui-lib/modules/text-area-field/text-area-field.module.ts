import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {TextAreaFieldComponent} from "./text-area-field.component";
import {FormsModule} from "@angular/forms";


@NgModule({
	declarations: [TextAreaFieldComponent],
	exports: [TextAreaFieldComponent],
	imports: [
		CommonModule,
		FormsModule
	]
})
export class TextAreaFieldModule {
}
