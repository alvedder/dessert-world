import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {BooleanFieldItem} from "../../models/boolean-field-item.model";

@Component({
	selector: "vui-checkbox-group-field",
	templateUrl: "checkbox-group-field.component.html",
	/*
		styleUrls: ["checkbox-group-field.component.less"]
	*/
})
export class CheckboxGroupFieldComponent implements OnInit {

	@Input()
	public items: BooleanFieldItem[] = [];

	/**
	 * this is a subset of items which should be checked
	 */
	@Input()
	public values: BooleanFieldItem[] = [];

	@Output()
	public valuesChange: EventEmitter<BooleanFieldItem[]> = new EventEmitter<BooleanFieldItem[]>();

	constructor() {
	}

	ngOnInit(): void {
	}

	public change(item: BooleanFieldItem): void {
		if (this.values && this.values.length !== 0) {
			if (this.itemChecked(item)) {
				this.values = this.values.filter(value => JSON.stringify(value) !== JSON.stringify(item))
			} else {
				this.values.push(item);
			}
		} else {
			this.values = [item];
		}
		this.valuesChange.emit(this.values);
	}

	public itemChecked(item: BooleanFieldItem): boolean {
		return this.values && this.values.some(value => JSON.stringify(value) === JSON.stringify(item));
	}

}
