import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CheckboxGroupFieldComponent} from "./checkbox-group-field.component";


@NgModule({
	declarations: [CheckboxGroupFieldComponent],
	exports: [CheckboxGroupFieldComponent],
	imports: [
		CommonModule
	]
})
export class CheckboxGroupFieldModule {
}
