import {Component, ElementRef, Input, ViewChild} from "@angular/core";
import {AbstractTextFieldComponent} from "../../components/abstract-text-field.component";

@Component({
	selector: "vui-phone-field",
	templateUrl: "phone-field.component.html",
	//	styleUrls: ["phone-field.component.less"]
})
export class PhoneFieldComponent extends AbstractTextFieldComponent {

	@Input()
	public phoneCode: string;

	@ViewChild("wrapper")
	protected wrapper: ElementRef;

	@ViewChild("input")
	protected input: ElementRef;

	constructor() {
		super();
	}
}
