import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {PhoneFieldComponent} from "./phone-field.component";
import {NgxMaskModule} from "ngx-mask";
import {FormsModule} from "@angular/forms";


@NgModule({
	declarations: [PhoneFieldComponent],
	exports: [PhoneFieldComponent],
	imports: [
		CommonModule,
		NgxMaskModule,
		FormsModule
	]
})
export class PhoneFieldModule {
}
