import {Component, Input, OnInit, TemplateRef} from "@angular/core";

@Component({
	selector: "vui-linked-block-group",
	templateUrl: "linked-block-group.component.html",
	// styleUrls: ["linked-block-group.component.less"]
})
export class LinkedBlockGroupComponent implements OnInit {

	@Input()
	public templates: TemplateRef<any>[];

	@Input()
	public context: any;

	constructor() {
	}

	ngOnInit(): void {
	}

}
