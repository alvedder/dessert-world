import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LinkedBlockGroupComponent} from "./linked-block-group.component";


@NgModule({
	declarations: [LinkedBlockGroupComponent],
	exports: [LinkedBlockGroupComponent],
	imports: [
		CommonModule
	]
})
export class LinkedBlockGroupModule {
}
