import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {LinkedBlockGroupComponent} from "./linked-block-group.component";

describe('LinkedBlockGroupComponent', () => {
  let component: LinkedBlockGroupComponent;
  let fixture: ComponentFixture<LinkedBlockGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkedBlockGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkedBlockGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
