import {Component, Input, OnInit} from "@angular/core";

@Component({
	selector: "vui-button",
	templateUrl: "button.component.html",
	// styleUrls: ["button.component.less"]
})
export class ButtonComponent implements OnInit {

	@Input()
	public content: string;

	@Input()
	public disabled: boolean;

	constructor() {
	}

	ngOnInit(): void {
	}

}
