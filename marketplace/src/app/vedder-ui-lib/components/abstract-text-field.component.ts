import {ElementRef, EventEmitter, Input, Output} from "@angular/core";

export abstract class AbstractTextFieldComponent {

	public inFocus: boolean = false;

	@Output()
	public valueChange: EventEmitter<string> = new EventEmitter<string>();

	@Input()
	public set value(value: string) {
		if (value.trim()) {
			this._value = value;
			this.valueChange.emit(value);
		}
	}

	public get value(): string {
		return this._value;
	}

	private _value: string = "";

	protected abstract input: ElementRef;

	protected abstract wrapper: ElementRef;
}
