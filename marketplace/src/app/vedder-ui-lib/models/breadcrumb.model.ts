export interface BreadcrumbItem {
	id: number;
	name: string;
	active: boolean;
	customClickAction?: () => void;
}
