import {Injectable} from "@angular/core";
import {Addon, Dessert, DessertTemplateGroups} from "../models/dessert.model";
import {ADDONS, CONTACTS, DELIVERY_WAYS, DESSERT_TEMPLATE_GROUPS, DESSERTS, IMAGES, ORDER_STAGES, PHONE_CODE} from "../../assets/data";
import {BreadcrumbItem} from "../vedder-ui-lib/models/breadcrumb.model";
import {Contact} from "../models/contact.model";
import {Delivery} from "../models/order.model";

@Injectable()
export class ContentService {

	constructor() {
	}

	public getDessertTemplateGroups(): DessertTemplateGroups {
		// TODO return from backend
		return DESSERT_TEMPLATE_GROUPS;
	}

	public getDessert(dessertId: string): Dessert {
		// TODO return from backend
		return JSON.parse(JSON.stringify(DESSERTS.find((dessert: Dessert) => dessert.id === dessertId)));
	}

	public getImagePath(imageId: string): string {
		// TODO return from backend
		return IMAGES.get(imageId);
	}

	public getOrderStages(): BreadcrumbItem[] {
		return JSON.parse(JSON.stringify(ORDER_STAGES));
	}

	public getContacts(): Contact[] {
		// TODO return from backend
		return JSON.parse(JSON.stringify(CONTACTS));
	}

	public getFeedback(): any {
		// TODO
	}

	public getPhoneCode(): string {
		// TODO return from backend
		return PHONE_CODE;
	}

	public getDeliveryWays(): Delivery[] {
		// TODO return from backend
		return JSON.parse(JSON.stringify(DELIVERY_WAYS));
	}

	public getDessertAddons(dessertId: string): Addon[] {
		// TODO return from backend
		return ADDONS.filter(addon => this.getDessert(dessertId).addonsIds.includes(addon.id));
	}
}
