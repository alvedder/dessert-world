import {Injectable, Renderer2, RendererFactory2} from "@angular/core";
import {ViewportScroller} from "@angular/common";
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";

@Injectable()
export class NavigatorService {

	private renderer: Renderer2;

	constructor(private route: ActivatedRoute,
				private router: Router,
				private viewportScroller: ViewportScroller,
				rendererFactory: RendererFactory2) {
		this.renderer = rendererFactory.createRenderer(null, null);
	}

	/**
	 * @param path
	 * @param elementId: element to scroll to
	 * @param options: TODO make out how to use it -)
	 */
	public redirectTo(path: string, elementId?: string, options?: NavigationExtras) {
		this.router.navigate([path], options).then(() => {
			if (elementId) {
				this.scrollToElement(elementId);
			}
		});
	}

	public scrollToElement(elementId: string): void {
		// in order to scroll correctly. 50ms is needed to allow the page to load all the content
		// TODO how to make the scrolling AFTER the page loaded
		setTimeout(() => {
			this.viewportScroller.scrollToAnchor(elementId);
		}, 50);
	}
}
