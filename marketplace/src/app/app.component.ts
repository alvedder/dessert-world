import {Component, HostListener} from "@angular/core";
import {NavigatorService} from "./services/navigator.service";

@Component({
	selector: "mp-app",
	templateUrl: "app.component.html",
})
export class AppComponent {

	public backToTopShown: boolean;

	constructor(private navigatorService: NavigatorService) {
	}

	public backToTop() {
		this.navigatorService.scrollToElement("header");
	}

	@HostListener("window:scroll")
	private scroll() {
		this.backToTopShown = 600 < window.scrollY;
	}
}
