import {Component, OnInit} from "@angular/core";

@Component({
	selector: "mp-footer",
	templateUrl: "./footer.component.html",
})
export class FooterComponent implements OnInit {

	public createrUrl = "https://gitlab.com/alexei-vedder";

	constructor() {
	}

	ngOnInit(): void {
	}

	public getCurrentYear(): number {
		return new Date().getFullYear();
	}
}
