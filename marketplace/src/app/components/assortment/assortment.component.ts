import {Component, ElementRef, OnInit, QueryList, ViewChildren} from "@angular/core";
import {DessertGroupTemplate, DessertTemplate, DessertTemplateGroups} from "../../models/dessert.model";
import {CardComponent} from "../../vedder-ui-lib/modules/card/card.component";
import {ContentService} from "../../services/content.service";
import {Router} from "@angular/router";

@Component({
	selector: "mp-assortment",
	templateUrl: "assortment.component.html",
})
export class AssortmentComponent implements OnInit {

	public dessertGroupModels: DessertTemplateGroups;

	/**
	 * Converted version of DessertTemplateGroups.
	 * This field is needed because it is not possible to use Map in *ngFor,
	 * so that this field represents maps (which DessertTemplateGroups type consists of) converted to arrays
	 */
	public dessertGroupModelsForTemplate: Array<[string, Array<[DessertGroupTemplate, Array<DessertTemplate>]>]>;

	public openedSubgroupId: string = null;

	@ViewChildren(CardComponent, {read: ElementRef})
	private dessertCards: QueryList<ElementRef>;

	constructor(private contentService: ContentService,
				private router: Router) {
	}

	ngOnInit(): void {
		this.initDessertGroupModels();
	}

	public goToDessertInfo(dessertId: string): void {
		if (dessertId) {
			this.router.navigate([`/dessert/${dessertId}`]);
		}
	}

	public getImagePath(imageId: string): string {
		return this.contentService.getImagePath(imageId);
	}

	public subgroupClick(subgroupId: string): void {
		if (this.openedSubgroupId === subgroupId) {
			this.openedSubgroupId = null;
		} else {
			this.openedSubgroupId = subgroupId;
		}
	}

	private initDessertGroupModels(): void {
		requestAnimationFrame(() => {
			this.dessertGroupModels = this.contentService.getDessertTemplateGroups();
			const dessertGroupModelsAsArray = Array.from(this.dessertGroupModels);
			dessertGroupModelsAsArray.forEach((group) => {
				// @ts-ignore
				group[1] = Array.from(group[1]);
			});
			// @ts-ignore
			this.dessertGroupModelsForTemplate = dessertGroupModelsAsArray;
		})
	}
}
