import {Component, OnInit} from "@angular/core";
import {ContentService} from "../../services/content.service";
import {Contact} from "../../models/contact.model";

@Component({
	selector: "mp-main-page-content",
	templateUrl: "./main-page-content.component.html",
})
export class MainPageContentComponent implements OnInit {

	public contacts: Contact[];

	constructor(private contentService: ContentService) {
	}

	public ngOnInit(): void {
		this.contacts = this.contentService.getContacts();
		// TODO init feedback
	}
}
