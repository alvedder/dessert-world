import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {ContentService} from "../../../services/content.service";
import {Addon} from "../../../models/dessert.model";
import {BooleanFieldItem} from "../../../vedder-ui-lib/models/boolean-field-item.model";

@Component({
	selector: "mp-addons-input",
	templateUrl: "addons-input.component.html",
})
export class AddonsInputComponent implements OnInit {

	@Input()
	public dessertId: string;

	@Input()
	public extraAddon: string;

	@Output()
	public extraAddonChange: EventEmitter<string> = new EventEmitter<string>();

	@Input()
	public chosenAddons: Addon[];

	@Output()
	public chosenAddonsChange: EventEmitter<Addon[]> = new EventEmitter<Addon[]>();

	public availableAddons: Addon[];

	public availableAddonsCheckboxItems: BooleanFieldItem[];

	public chosenAddonsCheckboxItems: BooleanFieldItem[];

	constructor(private contentService: ContentService) {
	}

	ngOnInit(): void {
		this.availableAddons = this.contentService.getDessertAddons(this.dessertId);
		this.availableAddonsCheckboxItems = this.addonsToCheckboxItems(this.availableAddons);
		this.chosenAddonsCheckboxItems = this.addonsToCheckboxItems(this.chosenAddons);
	}

	public checkboxesChange(values: BooleanFieldItem[]) {
		const ids: string[] = values.map(val => val.id);
		this.chosenAddons = this.availableAddons.filter((addon: Addon) => ids.includes(addon.id));
		this.chosenAddonsChange.emit(this.chosenAddons);
	}

	public extraAddonFieldChange(value: string) {
		this.extraAddonChange.emit(value);
	}

	private addonsToCheckboxItems(addons: Addon[]): BooleanFieldItem[] {
		if (addons && addons.length !== 0) {
			return addons.map((item) => {
				const text = item.description + " (" + item.price + "₽)";
				return {id: item.id, text};
			});
		} else return [];
	}
}
