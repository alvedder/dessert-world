import {Component, DoCheck, Input, KeyValueChanges, KeyValueDiffer, KeyValueDiffers, OnInit, TemplateRef, ViewChild} from "@angular/core";
import {Order} from "../../../models/order.model";
import {BreadcrumbItem} from "../../../vedder-ui-lib/models/breadcrumb.model";
import {ContentService} from "../../../services/content.service";
import {Addon} from "../../../models/dessert.model";

@Component({
	selector: "mp-order-state",
	templateUrl: "order-state.component.html",
})
export class OrderStateComponent implements OnInit, DoCheck {

	public templatesToShow: TemplateRef<any>[];

	public dessertImageUrl: string;

	public currentPrice: number = 0;

	@Input()
	public orderModel: Order;

	@Input()
	public currentStage: BreadcrumbItem;

	@ViewChild("dessert")
	private dessertTemplate: TemplateRef<any>;

	@ViewChild("size")
	private sizeTemplate: TemplateRef<any>;

	@ViewChild("addons")
	private addonsTemplate: TemplateRef<any>;

	@ViewChild("date")
	private dateTemplate: TemplateRef<any>;

	@ViewChild("delivery")
	private deliveryTemplate: TemplateRef<any>;

	@ViewChild("contacts")
	private contactsTemplate: TemplateRef<any>;

	private currentStageDiffer: KeyValueDiffer<any, any>;

	constructor(private keyValueDiffers: KeyValueDiffers,
				private contentService: ContentService) {
	}

	public ngOnInit(): void {
		const dessertImagePath = this.contentService.getImagePath(this.orderModel.dessert.imageIds[0]);
		this.dessertImageUrl = `url(\"${dessertImagePath}\")`;

		this.currentStageDiffer = this.keyValueDiffers.find(this.currentStage).create();
		requestAnimationFrame(() => {
			this.templatesToShow = [this.dessertTemplate];
		});
	}

	public ngDoCheck() {
		this.calculateCurrentPrice();

		const currentStageChanges: KeyValueChanges<any, any> = this.currentStageDiffer.diff(this.currentStage);

		if (currentStageChanges) {
			currentStageChanges.forEachChangedItem(() => {
				this.resolveTemplatesToShow();
			});
		}
	}

	private resolveTemplatesToShow() {
		requestAnimationFrame(() => {
			let templates = [];
			if (this.orderModel.dessert && this.orderModel.dessert.name) {
				templates.push(this.dessertTemplate);
			}
			if (this.orderModel.size && this.orderModel.size.size) {
				templates.push(this.sizeTemplate);
			}
			if ((this.orderModel.addons && this.orderModel.addons.length !== 0) ||
				(this.orderModel.extraAddon && this.orderModel.extraAddon !== "")) {
				templates.push(this.addonsTemplate);
			}
			if (this.orderModel.date) {
				templates.push(this.dateTemplate);
			}
			if (this.orderModel.delivery) {
				templates.push(this.deliveryTemplate);
			}
			this.templatesToShow = templates;
		});
	}

	private calculateCurrentPrice(): void {
		const dessertPrice = this.orderModel.size ? this.orderModel.size.price : 0;
		const addonsPrice = this.orderModel.addons && this.orderModel.addons.length !== 0 ?
			this.orderModel.addons.reduce((sum: number, addon: Addon) => {
				sum += addon.price;
				return sum;
			}, 0) : 0;
		const deliveryPrice = this.orderModel.delivery ? this.orderModel.delivery.price : 0;

		this.currentPrice = dessertPrice + addonsPrice + deliveryPrice;
	}
}
