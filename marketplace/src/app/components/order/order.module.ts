import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {OrderComponent} from "./order.component";
import {SizeInputComponent} from "./size-input/size-input.component";
import {AddonsInputComponent} from "./addons-input/addons-input.component";
import {DateInputComponent} from "./date-input/date-input.component";
import {DeliveryInputComponent} from "./delivery-input/delivery-input.component";
import {ContactsInputComponent} from "./contacts-input/contacts-input.component";
import {OrderStateComponent} from "./order-state/order-state.component";
import {RouterModule, Routes} from "@angular/router";
import {EmailFieldModule} from "../../vedder-ui-lib/modules/email-field/email-field.module";
import {PhoneFieldModule} from "../../vedder-ui-lib/modules/phone-field/phone-field.module";
import {CheckboxGroupFieldModule} from "../../vedder-ui-lib/modules/checkbox-group-field/checkbox-group-field.module";
import {RadioGroupFieldModule} from "../../vedder-ui-lib/modules/radio-group-field/radio-group-field.module";
import {BreadcrumbsModule} from "../../vedder-ui-lib/modules/breadcrumbs/breadcrumbs.module";
import {CalendarModule} from "../../vedder-ui-lib/modules/calendar/calendar.module";
import {ButtonModule} from "../../vedder-ui-lib/modules/button/button.module";
import {LinkedBlockGroupModule} from "../../vedder-ui-lib/modules/linked-block-group/linked-block-group.module";
import {TextAreaFieldModule} from "../../vedder-ui-lib/modules/text-area-field/text-area-field.module";


const routes: Routes = [
	{path: "", component: OrderComponent},
];

@NgModule({
	declarations: [
		OrderComponent,
		SizeInputComponent,
		AddonsInputComponent,
		DateInputComponent,
		DeliveryInputComponent,
		ContactsInputComponent,
		OrderStateComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		EmailFieldModule,
		PhoneFieldModule,
		CheckboxGroupFieldModule,
		RadioGroupFieldModule,
		BreadcrumbsModule,
		CalendarModule,
		ButtonModule,
		LinkedBlockGroupModule,
		TextAreaFieldModule
	]
})
export class OrderModule {
}
