import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {ContentService} from "../../../services/content.service";
import {Delivery} from "../../../models/order.model";
import {BooleanFieldItem} from "../../../vedder-ui-lib/models/boolean-field-item.model";

@Component({
	selector: "mp-delivery-input",
	templateUrl: "delivery-input.component.html",
})
export class DeliveryInputComponent implements OnInit {

	public deliveryWayRadioItems: BooleanFieldItem[];

	public deliveryWays: Delivery[];

	public chosenDeliveryItem: BooleanFieldItem;

	@Output()
	public chosenDeliveryChange: EventEmitter<Delivery> = new EventEmitter<Delivery>();

	@Input()
	public set chosenDelivery(value: Delivery) {
		this._chosenDelivery = value;
		if (value) {
			this.chosenDeliveryItem = this.deliveryWaysToRadioItems([value])[0];
		}
	}

	public get chosenDelivery(): Delivery {
		return this._chosenDelivery;
	}

	private _chosenDelivery: Delivery;

	constructor(private contentService: ContentService) {
	}

	public ngOnInit(): void {
		this.deliveryWays = this.contentService.getDeliveryWays();
		this.deliveryWayRadioItems = this.deliveryWaysToRadioItems(this.deliveryWays);
	}

	public itemChange(item: BooleanFieldItem): void {
		const chosenWay: Delivery = this.deliveryWays.find((delivery: Delivery) => delivery.id === item.id);
		this.chosenDeliveryChange.emit(chosenWay);
	}

	private deliveryWaysToRadioItems(deliveries: Delivery[]): BooleanFieldItem[] {
		if (deliveries && deliveries.length !== 0) {
			return deliveries.map((item) => {
				const text = item.text + " (" + item.price + "₽)";
				return {text, id: item.id};
			});
		}
		return [];
	}
}
