import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Size, SizeInfo} from "../../../models/dessert.model";

@Component({
	selector: "mp-size-input",
	templateUrl: "size-input.component.html",
})
export class SizeInputComponent implements OnInit {

	@Input()
	public sizes: SizeInfo[];

	@Input()
	public chosenSize: SizeInfo;

	@Output()
	public chosenSizeChange: EventEmitter<SizeInfo> = new EventEmitter<SizeInfo>();

	constructor() {
	}

	public ngOnInit(): void {
	}

	public sizeClick(size: Size): void {
		this.chosenSize = this.sizes.find((sizeInfo: SizeInfo) => sizeInfo.size === size);
		this.chosenSizeChange.emit(this.chosenSize);
	}

	public findSizeName(size: Size) {
		return this.sizes.find(item => item.size === size).name;
	}
}
