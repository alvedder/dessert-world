import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Contacts} from "../../../models/order.model";
import {ContentService} from "../../../services/content.service";

@Component({
	selector: "mp-contacts-input",
	templateUrl: "contacts-input.component.html",
})
export class ContactsInputComponent implements OnInit {

	@Output()
	public contactsChange: EventEmitter<Contacts> = new EventEmitter<Contacts>();

	public phoneCode: string;

	public get contacts(): Contacts {
		return this._contacts;
	}

	@Input()
	public set contacts(value: Contacts) {
		this._contacts = value;
		if (this._contacts) {
			this.contactsChange.emit(this._contacts);
		}
	}

	private _contacts: Contacts;

	constructor(private contentService: ContentService) {
	}

	public ngOnInit(): void {
		this.phoneCode = this.contentService.getPhoneCode();
	}
}
