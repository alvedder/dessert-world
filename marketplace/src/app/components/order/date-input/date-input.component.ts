import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {addDays, format} from "date-fns";
import {ru} from "date-fns/locale";


@Component({
	selector: "mp-date-input",
	templateUrl: "date-input.component.html",
})
export class DateInputComponent implements OnInit {

	public readonly minAvailableDate: Date = addDays(new Date(), 2);

	public readonly locale: Locale = ru;

	@Input()
	public unavailableDates: Date[] = [];

	@Output()
	public chosenDateChange: EventEmitter<Date> = new EventEmitter<Date>();

	public formattedChosenDate: string;

	@Input()
	public set chosenDate(value: Date) {
		this._chosenDate = value;
		if (this._chosenDate) {
			this.chosenDateChange.emit(this._chosenDate);
			this.formattedChosenDate = format(this._chosenDate, "dd MMMM yyyy", {locale: this.locale});
		}
	}

	public get chosenDate(): Date {
		return this._chosenDate;
	}

	private _chosenDate: Date;

	constructor() {
	}

	public ngOnInit(): void {
	}
}
