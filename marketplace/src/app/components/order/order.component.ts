import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {ContentService} from "../../services/content.service";
import {BreadcrumbItem} from "../../vedder-ui-lib/models/breadcrumb.model";
import {OrderStage} from "../../models/order-stage.enum";
import {Addon, Dessert, SizeInfo} from "../../models/dessert.model";
import {Contacts, Delivery, Order} from "../../models/order.model";
import {NavigatorService} from "../../services/navigator.service";

@Component({
	selector: "mp-order",
	templateUrl: "order.component.html",
})
export class OrderComponent implements OnInit {

	public orderStages: BreadcrumbItem[];

	public currentStage: BreadcrumbItem;

	public get model(): Order {
		return this._model;
	}

	public set model(value: Order) {
		this._model = value;
	}

	/**
	 * for accessing OrderStage within the template
	 */
	get OrderStage() {
		return OrderStage;
	}

	public orderAccepted: boolean = false;

	private _model: Order = new class implements Order {
		public addons: Addon[];
		public contacts: Contacts = new class implements Contacts {
			public email: string;
			public phone: string;
		};
		public date: Date;
		public delivery: Delivery;
		public dessert: Dessert;
		public extraAddon: string = "";
		public size: SizeInfo;
	};

	constructor(private router: Router,
				private route: ActivatedRoute,
				private contentService: ContentService,
				public navigatorService: NavigatorService) {
	}

	public ngOnInit(): void {
		this.orderStages = this.contentService.getOrderStages();
		this.getStage(OrderStage.Cake).customClickAction = () => {
			this.redirectToMainPage();
		};
		this.currentStage = this.getStage(OrderStage.Size);
		this.initModel();
	}

	public onBackClick(): void {
		switch (this.currentStage.id) {
			case OrderStage.Cake: {
				break;
			}
			case OrderStage.Size: {
				this.redirectToMainPage();
				break;
			}
			default: {
				this.currentStage = this.getStage(this.currentStage.id - 1);
			}
		}
	}

	public onNextClick(): void {
		switch (this.currentStage.id) {
			case OrderStage.Cake: {
				break;
			}
			case OrderStage.Contacts: {
				this.acceptOrder();
				break;
			}
			default: {
				this.currentStage = this.getStage(this.currentStage.id + 1);
				this.currentStage.active = true;
			}
		}
	}

	public nextButtonsDisabled(): boolean {
		switch (this.currentStage.id) {
			case OrderStage.Cake: {
				return false;
			}
			case OrderStage.Size: {
				return !this.model.size;
			}
			case OrderStage.Addons: {
				return false;
			}
			case OrderStage.Date: {
				return !this.model.date;
			}
			case OrderStage.Delivery: {
				return !this.model.delivery;
			}
			case OrderStage.Contacts: {
				return !this.model.contacts.phone;
			}
		}
	}

	public resolveNextButtonContent(): string {
		switch (this.currentStage.id) {
			case OrderStage.Cake: {
				return "";
			}
			case OrderStage.Contacts: {
				return "Сделать заказ";
			}
			default: {
				return "Далее";
			}
		}
	}

	public resolveBackButtonContent(): string {
		switch (this.currentStage.id) {
			case OrderStage.Cake: {
				return "";
			}
			case OrderStage.Size: {
				return "Выбор десерта";
			}
			default: {
				return "Назад";
			}
		}
	}

	private getStage(id: OrderStage): BreadcrumbItem {
		return this.orderStages.find((stage: BreadcrumbItem) => stage.id === id);
	}

	private initModel(): void {
		const dessertId = this.route.snapshot.paramMap.get("dessertId");
		this.model.dessert = this.contentService.getDessert(dessertId);
		this.model.size = this.model.dessert.sizes.find(sizeInfo => sizeInfo.size === "medium");
	}

	private acceptOrder(): void {
		// asynchronously send email
		this.orderAccepted = true;
	}

	private redirectToMainPage(): void {
		if (window.confirm("Текущий заказ будет потерян. Вернуться на страницу выбора десертов?")) {
			this.navigatorService.redirectTo("", "assortment");
		}
	}
}
