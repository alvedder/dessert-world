import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {ContentService} from "../../services/content.service";
import {Dessert} from "../../models/dessert.model";

@Component({
	selector: "mp-dessert-info",
	templateUrl: "dessert-info.component.html",
})
export class DessertInfoComponent implements OnInit {

	get dessert(): Dessert {
		return this._dessert;
	}

	set dessert(value: Dessert) {
		this._dessert = value;
	}

	private _dessert: Dessert;

	constructor(private router: Router,
				private route: ActivatedRoute,
				private contentService: ContentService) {
	}

	public ngOnInit(): void {
		const dessertId = this.route.snapshot.paramMap.get("dessertId");
		this.dessert = this.contentService.getDessert(dessertId);
	}

	public goToOrder() {
		this.router.navigate([`order`], {relativeTo: this.route});
	}
}
