import {Component, HostListener, OnInit} from "@angular/core";
import {NavigatorService} from "../../services/navigator.service";

@Component({
	selector: "mp-header",
	templateUrl: "header.component.html",
})
export class HeaderComponent implements OnInit {

	public burgerMenu: boolean;

	public burgerMenuOpened: boolean;

	private mediaMaxWidth: number = 910;

	constructor(private navigatorService: NavigatorService) {
	}

	public ngOnInit(): void {
		this.resolveMenuType();
	}

	public burgerMenuClick() {
		if (this.burgerMenu) {
			this.burgerMenuOpened = !this.burgerMenuOpened;
		}
	}

	public redirectTo(path, elementId?, options?): void {
		this.burgerMenuOpened = false;
		this.navigatorService.redirectTo(path, elementId, options)
	}

	@HostListener("window:resize")
	private resize() {
		this.resolveMenuType();
	}

	private resolveMenuType(): void {
		this.burgerMenuOpened = false;
		this.burgerMenu = window.innerWidth <= this.mediaMaxWidth;
	}
}
