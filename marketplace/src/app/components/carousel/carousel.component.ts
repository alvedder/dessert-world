import {Component, Input, OnInit} from "@angular/core";
import {ContentService} from "../../services/content.service";
import {IImage} from "ng-simple-slideshow";

@Component({
	selector: "mp-carousel",
	templateUrl: "carousel.component.html",
})
export class CarouselComponent implements OnInit {

	@Input()
	public imageIds: string[];

	public imagesUrls: IImage[];

	public fullscreen: boolean = false;

	constructor(private contentService: ContentService) {
	}

	public ngOnInit(): void {
		this.initImages();
	}

	private initImages(): void {
		this.imagesUrls = this.imageIds.map((imageId: string) => {
			return {
				url: this.contentService.getImagePath(imageId),
				clickAction: () => this.fullscreen = !this.fullscreen
			};
		});
	}
}
